import AutoImport from 'unplugin-auto-import/vite';
import Components from 'unplugin-vue-components/vite';
import { ElementPlusResolver } from 'unplugin-vue-components/resolvers';

export function configElementPlusAutoImport() {
  return AutoImport({
    resolvers: [ElementPlusResolver()],
    dts: 'types/auto-imports.d.ts',
  });
}

export function configElementPlusComponents() {
  return Components({
    dirs: 'src/views',
    resolvers: [ElementPlusResolver()],
    dts: 'types/components.d.ts',
  });
}
