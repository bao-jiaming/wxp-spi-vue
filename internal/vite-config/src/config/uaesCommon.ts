import { presetTypography, presetUno, presetIcons, transformerDirectives } from 'unocss';
import UnoCSS from 'unocss/vite';
import { type UserConfig } from 'vite';

const commonConfig: UserConfig = {
  server: {
    host: true,
  },
  esbuild: {
    drop: ['debugger'],
  },
  build: {
    reportCompressedSize: false,
    chunkSizeWarningLimit: 1500,
    rollupOptions: {
      // TODO: Prevent memory overflow
      maxParallelFileOps: 3,
    },
  },
  plugins: [
    UnoCSS({
      presets: [presetUno(), presetTypography(), presetIcons()],
      transformers: [transformerDirectives()],
      shortcuts: [
        {
          'common-flex': 'flex justify-center items-center',
          'cm-flex': 'flex justify-center items-center',
          'common-flex-col': 'flex justify-center items-center flex-col',
          'cm-flex-col': 'flex justify-center items-center flex-col',
          'border-dev': 'border-1px border-red-500 border-dashed',
        },
        [/^bd-(.*)$/, ([, c]) => `border-${c}-500 border-1px border-dashed`],
      ],
    }),
  ],
};
export function getUaesCommonConfig(isBuild: boolean): UserConfig {
  console.log("🚀 🔶 getUaesCommonConfig 🔶 isBuild=>", isBuild)
  if (isBuild) {
    return commonConfig;
  } else {
    //非build默认打开打印和调试
    commonConfig.esbuild = {};
    return commonConfig;
  }
}
