<div align="center"> <br> 
<h1>TEF Vben Ⅱ </h1>
</div>
Mail Mode:Domain Account

Offline Mode:TEF/123456

## Preparation

- [node](http://nodejs.org/) and [git](https://git-scm.com/) - Project development environment
- [Vite](https://vitejs.dev/) - Familiar with vite features
- [Vue3](https://v3.vuejs.org/) - Familiar with Vue basic syntax
- [TypeScript](https://www.typescriptlang.org/) - Familiar with the basic syntax of `TypeScript`
- [Es6+](http://es6.ruanyifeng.com/) - Familiar with es6 basic syntax
- [Vue-Router-Next](https://next.router.vuejs.org/) - Familiar with the basic use of vue-router
- [Ant-Design-Vue](https://antdv.com/docs/vue/introduce-cn/) - ui basic use
- [Mock.js](https://github.com/nuysoft/Mock) - mockjs basic syntax

## Install and use

- Get the project code

```bash
WxP
git clone http://174.35.64.98/yuke.lu/vben-base2.git

C7N
git clone http://code.c7n.uaes.com/tf-wxp-portal/wxp-template-vue.git
```

- Installation dependencies

```bash
cd {folder_name}

pnpm install

```

- run

```bash
pnpm dev
```

- build

```bash
pnpm build
```
## License

[MIT © Vben-2020](./LICENSE)
