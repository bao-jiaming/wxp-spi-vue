/*
 * @Description:
 * @version:
 * @Author: Bao Jiaming
 * @Date: 2024-09-14 08:35:45
 * @LastEditTime: 2024-09-14 08:54:56
 * @FilePath: \src\views\spi\dashboard\const.ts
 */
type TableDataType = {
  // apPassedDate	AP通过日期	string(date)
  apPassedDate: string;
  // apStatus	AP状态	string
  apStatus: string;
  // assemblyFinishDate	装配完成日期	string(date)
  assemblyFinishDate: string;
  // beginDate	计划开始日期	string(date)
  beginDate: string;
  // customerName	客户名称	string
  customerName: string;
  // dllFinishDate	DLL完成日期	string(date)
  dllFinishDate: string;
  // orderNo	订单号	string
  orderNo: string;
  // pdta	pdta	string
  pdta: string;
  // planQuantity	计划数量	integer(int32)
  planQuantity: string;
  // projectName	项目名称	string
  projectName: string;
  // remark	备注	string
  remark: string;
  // storeDate	入库完成日期	string(date)
  storeDate: string;
  // storeQuantity	入库数量	integer(int32)
  storeQuantity: string;
  // testFinishDate	测试完成日期	string(date)
  testFinishDate: string;
};

export type { TableDataType };
