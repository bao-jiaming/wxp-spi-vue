import { computed } from 'vue';
import { min, round } from 'lodash-es';
import { useElementSize } from '@vueuse/core';
import type { Ref } from 'vue';

/**
 * @author: yuke.lu
 * @description:
 * @param {Ref} elRef 尺寸不固定的容器
 * @param {number} targetWidth 固定比例的内容宽度
 * @param {number} targetHeight 固定比例的内容高度
 * @return {*}
 */
export function useResizeScale(
  elRef: Ref<HTMLDivElement>,
  targetWidth: number,
  targetHeight: number,
): any {
  const { width, height } = useElementSize(elRef);
  let scaleratio = computed(() => {
    let widthRatio = width.value / targetWidth;
    // console.log('🚀 🔶 scaleratio 🔶 widthRatio=>', widthRatio);
    let heightRatio = height.value / targetHeight;
    if (widthRatio == 0 || heightRatio == 0) {
      return 1;
    }
    return round(min([widthRatio, heightRatio])!, 6);
  });
  return { width, height, scaleratio };
}
