/*
 * 全局自定义快捷键配置 
 */
import { watch, unref } from 'vue';
import { useMagicKeys } from '@vueuse/core';
import { baseHandler } from '/@/layouts/default/setting/handler';
import { useRootSetting } from '/@/hooks/setting/useRootSetting';
export function useHotkey() {
  const keys = useMagicKeys();
  //内容全屏的快捷键
  const ctrlF11 = keys['Ctrl+f11'];
  const { getFullContent } = useRootSetting();
  watch(ctrlF11, (v) => {
    if (v) {
      baseHandler(26, !unref(getFullContent));
    }
  });
}
