/*
 * @Description:
 * @version:
 * @Author: Bao Jiaming
 * @Date: 2024-08-29 16:33:46
 * @LastEditTime: 2024-09-12 12:53:11
 * @FilePath: \src\router\routes\modules\spi.ts
 */
import type { AppRouteModule } from '/@/router/types';

import { LAYOUT } from '/@/router/constant';
import { t } from '/@/hooks/web/useI18n';

const myRoute: AppRouteModule = {
  path: '/spi',
  name: 'Spi',
  component: LAYOUT,
  redirect: '/spi/dashboard',
  meta: {
    orderNo: 10,
    icon: 'ion:grid-outline',
    title: t('Home'),
    hideMenu: false,
  },
  children: [
    {
      path: 'dashboard',
      name: 'Dashboard',
      component: () => import('/@/views/spi/dashboard/index.vue'),
      meta: {
        // affix: true,
        title: t('样品车间生产信息'),
      },
    },
    {
      path: 'productInfo',
      name: 'productInfo',
      component: () => import('/@/views/spi/manage/productInfo/index.vue'),
      meta: {
        // affix: true,
        title: '产品信息',
        ignoreKeepAlive: false,
      },
    },
    {
      path: 'orderInfo',
      name: 'orderInfo',
      component: () => import('/@/views/spi/manage/orderInfo/index.vue'),
      meta: {
        // affix: true,
        title: '订单信息',
        ignoreKeepAlive: false,
      },
    },
    {
      path: 'createPlan',
      name: 'createPlan',
      component: () => import('/@/views/spi/manage/createPlan/index.vue'),
      meta: {
        // affix: true,
        title: '生产计划',
        ignoreKeepAlive: false,
      },
    },
    {
      path: 'createProcess',
      name: 'createProcess',
      component: () => import('/@/views/spi/manage/createProcess/index.vue'),
      meta: {
        // affix: true,
        title: '生产过程',
        ignoreKeepAlive: false,
      },
    },
  ],
};

export default myRoute;
