/*
 * @Description:
 * @version:
 * @Author: Bao Jiaming
 * @Date: 2024-08-29 13:53:28
 * @LastEditTime: 2024-09-12 13:57:10
 * @FilePath: \src\utils\http\axios\httpInstance.ts
 */
import { createAxios } from './index';
import { useGlobSetting } from '/@/hooks/setting';
import { LoginModeEnum } from '/@/enums/loginModeEnum';
const globSetting = useGlobSetting();

//tableau票证获取服务
export const tableauHttp = createAxios({
  requestOptions: {
    apiUrl: 'http://174.35.64.19:8080/ascp-spring-back-ticket',
  },
});

// WxP Demo用上传文件接口
export const uploadServiceHttp = createAxios({
  requestOptions: {
    apiUrl: 'http://localhost:8080',
  },
});

// WxP Demo用下载文件接口
export const downloadServiceHttp = createAxios({
  requestOptions: {
    apiUrl: 'http://localhost:8080',
  },
});

//WxP 用户登录服务
export const authHttp = (() => {
  if ([LoginModeEnum.ONLINE, LoginModeEnum.SSO].includes(globSetting?.uaesLoginMode)) {
    return createAxios({
      requestOptions: {
        apiUrl: 'https://sso-be.wxp.dev.dservice.uaes.com',
      },
    });
  } else {
    return createAxios({
      requestOptions: {
        //mock
        apiUrl: '/basic-api',
      },
    });
  }
})();

export const spiHttp = createAxios({
  requestOptions: {
    // apiUrl:'http://wpxtef.a1.luyouxia.net:20186',
    apiUrl:'https://spi-be.wxp.dev.dservice.uaes.com',
  },
});
