import type { App } from 'vue';
import { Button } from './Button';
import {
  Input,
  Tree,
  TreeSelect,
  Layout,
  DatePicker,
  TimeRangePicker,
  RangePicker,
  TimePicker,
  Table,
  Form,
  FormItem,
  Row,
  Col,
  Spin,
  Select,
  Popconfirm,
  Modal,
  Cascader,
  Upload,
  Card,
  Carousel,
  Image,
} from 'ant-design-vue';

import Vue3Marquee from 'vue3-marquee'

// 按需引入
export function registerGlobComp(app: App) {
  app
    .use(Input)
    .use(Tree)
    .use(TreeSelect)
    .use(Image)
    .use(Button)
    .use(Layout)
    .use(Carousel)
    .use(DatePicker)
    .use(Table)
    .use(Form)
    .use(FormItem)
    .use(Row)
    .use(Spin)
    .use(Popconfirm)
    .use(Modal)
    .use(Cascader)
    .use(Upload)
    .use(Col)
    .use(Card)
    .use(RangePicker)
    .use(TimePicker)
    .use(TimeRangePicker)
    .use(Vue3Marquee)
    .use(Select);
}
