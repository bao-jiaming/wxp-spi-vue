import {spiHttp} from '/@/utils/http/axios/httpInstance';



//订单下拉框
export const getOrderNoList = (params) =>
  spiHttp.get({
    url: '/order/options',
    params,
  });

//零件号下拉框
export const getPartNoList = (params) =>
  spiHttp.get({
    url: '/product/options',
    params,
  });

//客户号下拉框
export const getCustomerList = (params) =>
  spiHttp.get({
    url: '/const/options/CUSTOMER',
    params,
  });

//阶段下拉框
export const getPhaseList = (params) =>
  spiHttp.get({
    url: '/const/options/PHASE',
    params,
  });

//产品类型下拉框
export const getTypeList = (params) =>
  spiHttp.get({
    url: '/const/options/TYPE',
    params,
  });

//外形下拉框
export const getShapeList = (params) =>
  spiHttp.get({
    url: '/const/options/SHAPE',
    params,
  });

//状态下拉框
export const getStatusList = (params) =>
  spiHttp.get({
    url: '/const/options/STATUS',
    params,
  });

//AP_状态下拉框
export const getAPStatusList = (params) =>
spiHttp.get({
  url: '/const/options/AP_STATUS',
  params,
});

//计划页面接口
export const getPlanPageList = (params) =>
spiHttp.get({
  url: '/plan/page',
  params,
});

export const editPlan = (params) =>
spiHttp.put({
  url: '/plan',
  params,
});

export const addPlan = (params) =>
spiHttp.post({
  url: '/plan',
  params,
});

export const deletePlan = (params) =>
spiHttp.delete({
  url: `/plan/${params}`,
});

//过程页面接口
export const getProcessPageList = (params) =>
spiHttp.get({
  url: '/process/page',
  params,
});

export const editProcess = (params) =>
spiHttp.put({
  url: '/process',
  params,
});

//订单页面接口
export const getOrderPageList = (params) =>
spiHttp.get({
  url: '/order/page',
  params,
});

export const addOrder = (params) =>
spiHttp.post({
  url: '/order',
  params,
});

export const editOrder = (params) =>
spiHttp.put({
  url: '/order',
  params,
});

export const deleteOrder = (params) =>
spiHttp.delete({
  url: `/order/${params}`,
});

//产品页面接口
export const getProductPageList = (params) =>
spiHttp.get({
  url: '/product/page',
  params,
});

export const addProduct = (params) =>
spiHttp.post({
  url: '/product',
  params,
});

export const editProduct = (params) =>
spiHttp.put({
  url: '/product',
  params,
});

export const deleteProduct = (params) =>
spiHttp.delete({
  url: `/product/${params}`,
});

//首页接口
//滚动表格接口
export const getMarqueeList = (params) =>
spiHttp.get({
  url:'/process/list',
  params,
})

//饼图接口
export const getPieChartList = (params) =>
spiHttp.get({
  url:'/index/order/type/sum',
  params,
})

//柱状图接口
export const getBarChartList = (params) =>
spiHttp.get({
  url:'/index/production/sum',
  params,
})

//订单总数接口
export const getTotalOrder = (params) =>
spiHttp.get({
  url:'/index/order/total',
  params,
})

//零件总数接口
export const getTotalPart = (params) =>
spiHttp.get({
  url:'/index/part/sum',
  params,
})


