export interface MixSubmitModel {
  field1?: string;
  field2?: string;
  fileList?: File[];
}
