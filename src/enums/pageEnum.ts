/*
 * @Description: 
 * @version: 
 * @Author: Bao Jiaming
 * @Date: 2024-08-29 13:53:28
 * @LastEditTime: 2024-08-29 16:38:26
 * @FilePath: \src\enums\pageEnum.ts
 */
export enum PageEnum {
  // basic login path
  BASE_LOGIN = '/login',
  // basic home path
  BASE_HOME = '/spi/dashboard',
  // error page path
  ERROR_PAGE = '/exception',
  // error log page path
  ERROR_LOG_PAGE = '/error-log/list',
}
export const PageWrapperFixedHeightKey = 'PageWrapperFixedHeight';
