
// login mode
export enum LoginModeEnum {
  OFFLINE = 'offline',
  ONLINE = 'online',
  SSO = 'sso',
}

